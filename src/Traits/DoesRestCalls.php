<?php

namespace TsLib\Rest\Traits;

use TsLib\Rest\Services\NetsuiteRest;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Log;

trait DoesRestCalls
{
	static private function doGet(NetsuiteRest $ns, $path)
	{
		return $ns->doRequest('GET', $path, json_encode([]));
	}

	static public function find($id)
	{
		//crear instance de la clase que la llama y regresarla

		$ns = new NetsuiteRest();
		$result = self::doGet($ns, sprintf(self::$path.'/%s', $id));
		if(isset($result["status"]) && $result["status"])
		{
			return $result["data"];
		}
		else
		{
			return null;
		}
	}

	static private function doPost(NetsuiteRest $ns, $path, $json)
	{
		return $ns->doRequest('POST', $path, $json);
	}

	static public function save_from_data($data)
	{
		if(is_array($data))
		{
			$data = json_encode($data);
		}

		if(!is_string($data))
		{
			Log::error("No se recibio un objeto data correcto en JSON", $data);
			return false;
		}

		json_decode($data);
		if(json_last_error() !== JSON_ERROR_NONE)
		{
			$data = json_encode($data);
		}

		$result = self::doPost($this->ns, self::$path, $data);
		if(isset($result["status"]) && $result["status"])
		{
			Log::debug(sprintf("Resultado de save_from_data %s: %s", get_class($this), $result["data"]));
			return $result["data"];
		}
		else
		{
			return false;
		}
	}

	public function save()
	{
		$json = $this->makeJsonFromId($this->id);
		$result = self::doPost($this->ns, self::$path, $json);

		if(isset($result["status"]) && $result["status"])
		{
			return $result["data"];
		}
		else
		{
			Log::debug(sprintf("Resultado de save %s: %s", get_class($this), json_encode($result["data"])));
			return false;
		}
	}

	public function makeJsonFromId()
	{
        if($this->id == null)
        {
            Log::error(sprintf("Se intento generar un JSON de %s sin id definido", get_class($this)));
            return false;
        }

        if(!($this->record['instance'] instanceof Model))
        {
            $this->record['instance'] = $this->record['class']::find($this->id);
        }
        
        if($this->record['instance'] == null)
        {
        	$model = new $this->record['class']();
        	Log::error(sprintf("No se encontro el registro %s de tipo %s en tabla %s", $this->id, get_class($this), $model->getTable()));
        	return false;
        }

		return $this->makeJson($this->record['instance']);
	}

	public function makeJson()
	{
		return json_encode([]);
	}
}