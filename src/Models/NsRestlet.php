<?php

namespace TsLib\Rest\Models;

use Illuminate\Database\Eloquent\Model;

class NsRestlet extends Model
{
    protected $table = "ns_restlets";
}
