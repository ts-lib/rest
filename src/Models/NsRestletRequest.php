<?php

namespace TsLib\Rest\Models;

use Illuminate\Database\Eloquent\Model;

class NsRestletRequest extends Model
{
    //
    protected $table = 'ns_restlet_requests';
}
