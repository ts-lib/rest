<?php

namespace TsLib\Rest;

use Illuminate\Support\ServiceProvider;

class RestServiceProvider extends ServiceProvider
{
	public function boot()
	{
		$this->publishes([
		        __DIR__.'/config/rest.php' => config_path('ts-lib-rest.php'),
		    ]);
	}

	public function register()
	{
		//$this->loadRoutesFrom(__DIR__.'/routes.php');
	}
}