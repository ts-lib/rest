<?php

namespace TsLib\Rest\NsRest\Fields\Traits;

trait HasOperator
{
	protected $default_operator = '';
	protected $operators = [];
}