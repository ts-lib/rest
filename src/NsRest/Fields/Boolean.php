<?php

namespace TsLib\Rest\NsRest\Fields;

class Boolean extends FieldBase
{
	protected $default_operator = 'IS';
	protected $operators = ['IS', 'IS_NOT'];
}