<?php

namespace TsLib\Rest\NsRest\Fields;
use TsLib\Rest\NsRest\Fields\Traits\HasOperator;

class FieldBase
{
	use HasOperator;
	
	public static function build($field, $value, $operator = null)
	{
		if($operator == null)
		{
			$operator = (new static)->default_operator;
		}

		if(!in_array($operator, (new static)->operators))
		{
			throw new \Exception(sprintf("No se permite el operador %s para el campo %s de tipo %s", $operator, $field, get_class(new static)));
		}

		if(is_array($value))
		{
			$value = '['.implode(', ', $value).']';
		}

		return $field.' '.$operator.' '.$value;
	}
}