<?php

namespace TsLib\Rest\NsRest;

use TsLib\Rest\Services\NetsuiteRest;
use Illuminate\Support\Facades\Log;

use TsLib\Rest\NsRest\Fields\Boolean as FieldBoolean;

class NsBase
{
	static protected $path = '';
	static private $base_rest_services = 'https://%s.suitetalk.api.netsuite.com/services/rest/';
	protected $rest_path = '';
	protected $filters = [];

	protected $boolean = [];

	protected $id = null;
	protected $record = ['instance' => null, 'class' => null];

	protected $ns = null;

	public function __construct($options = [])
	{
		$this->ns = new NetsuiteRest();
		if(isset($options['record']))
		{
			$this->record['instance'] = $options['record'];
		}
		
		if(isset($options['id']))
		{
			$this->id = $options['id'];
		}
	}

	private function ns()
	{
		if(get_class($this->ns) != NetsuiteRest::class)
		{
			$this->ns = new NetsuiteRest();
		}

		return $this->ns;
	}

	private static function _ns()
	{
		return (new static)->ns();
	}

	private function get_path($extra = '')
	{
		if(get_class($this) == 'TsLib\\Rest\\Ns')
		{
			Log::error("No se puede hace el path de clase Base");
			return false;
		}

		$class = substr(get_class($this),  strrpos(get_class($this), '\\') + 1);

		return sprintf(self::$base_rest_services.'%s%s%s', strtolower(str_replace('_', '-', config('ts-lib-rest.account'))), $this->rest_path, $class, $extra);
	}

	private static function _get_path($extra = '')
	{
		return (new static)->get_path($extra);		
	}

	private static function _get_filters()
	{
		$return = [];
		foreach((new static)->boolean as $key => $filter)
		{
			if(is_numeric($key))
				$return[$filter] = ['class' => FieldBoolean::class];
			else
				$return[$key] = ['class' => FieldBoolean::class, 'ns_filter' => $filter];
		}

		return $return;
	}

	private static function _build_field($fieldname, $value, $operator = null)
	{
		$filters = self::_get_filters();

		if(!isset($filters[$fieldname]))
		{
			throw new \Exception(sprintf('No se encontro el filtro %s en la clase %s', $fieldname, get_class(new static)));
		}

		$index = $fieldname;
		if(isset($filters[$fieldname]['ns_filter']))
			$fieldname = $filters[$fieldname]['ns_filter'];

		return $filters[$index]['class']::build($fieldname, $value, $operator);
	}

	public static function all()
	{
		return self::_ns()->get(self::_get_path());
	}

	public static function find($id)
	{
		return self::_ns()->get(self::_get_path('/'.$id));
	}

    public static function where($filters = [])
    {
        $parsed = [];
        $query = [];
        foreach($filters as $field => $value)
        {
        	//quiere decir que no se mando formato 'field' => 'value'
        	//suponemos que es un arreglo de 2 o 3 elementos
        	if(is_numeric($field))
        	{
        		if(!is_array($value))
        		{
        			throw new \Exception(sprintf("Se definio el filtro %s sin valor para la clase %s", $value, get_class(new static)));
        		}

        		if(count($value) == 1)
        		{
        			throw new \Exception(sprintf("Se definio el filtro %s sin valor para la clase %s", $value[0], get_class(new static)));
        		}

        		if(count($value) == 2)
        		{
        			$field = $value[0];
        			$value = $value[1];
        			$operator = null;
        		}
        		else
        		{
        			$field = $value[0];
        			$operator = $value[1];
        			$value = $value[2];
        		}
        	}
        	else
        	{
        		$operator = null;
        	}

        	$query[] = self::_build_field($field, $value, $operator);
        }

        return self::_ns()->get(self::_get_path(), ['q' => implode(' AND ', $query)]);
	}
}