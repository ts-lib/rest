<?php

namespace TsLib\Rest\NsRest\Records\Sales;

use TsLib\Rest\NsRest\Records\Record;
use TsLib\Rest\Traits\DoesRestCalls;

use Illuminate\Support\Facades\Log;

class Contact extends Record
{
	use DoesRestCalls;
	static protected $path = 'record/v1/contact';
    //protected $record = ['instance' => null, 'class' => PendingSaleOrder::class];

}