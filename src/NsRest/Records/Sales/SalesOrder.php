<?php

namespace TsLib\Rest\NsRest\Records\Sales;

use TsLib\Rest\NsRest\Records\Record;
use TsLib\Rest\Traits\DoesRestCalls;

use TsLib\ModelsSales\PendingSaleOrder;
use TsLib\ModelsSales\CustomerAddress;
use TsLib\ModelsItems\Item;
use TsLib\ModelsItems\ItemMember;

use Illuminate\Support\Facades\Log;

class SalesOrder extends Record
{
	use DoesRestCalls;
	static protected $path = 'record/v1/salesOrder';
    protected $record = ['instance' => null, 'class' => PendingSaleOrder::class];

	public function makeJson()
	{
        $pedido = $this->record['instance'];
		//Validar que $record sea record
        if(!($pedido instanceof PendingSaleOrder))
        {
            Log::error("El valor de record['instance'] no es de tipo PendingSaleOrder");
            return json_encode([]);
        }

        $search = ["\n","\r",'"'];
        $address = CustomerAddress::find($pedido->address_id);
        $createSaleOrder = [
            "entity" => ["id" => $pedido->customer_id],
            "memo" => "Forma de pago: ".$this->ns::wrapperAcentos($pedido->type_payment)." Comentarios: ".str_replace($search," ",$this->ns::wrapperAcentos($pedido->comments)),
            "location" => ["id" => $pedido->location_id],
            "currency" => ["id" => $pedido->currency],
            "custbody_comentarios_envio" =>"Comentarios: ".str_replace($search," ",$this->ns::wrapperAcentos($pedido->comments)),
            "custbody11" => $pedido->delivery,
            "custbody23" => true,
            "custbody_ped_usuario_levanto_portal" => $pedido->user_id,
            "custbody_referencia_pago_paypal" => $pedido->paypal_code,
            "custbody_pedido_correo_cliente" => $pedido->email,
            "custbody_pedido_telefono_cliente" => $pedido->phone
        ];
        if($pedido->option_payment!=""){
            $createSaleOrder["custbody_so_procesador_pagos"] = $pedido->option_payment;
        }
        if($pedido->paypal_code!='{}' && $pedido->paypal_code!=""){
            if(strpos($pedido->type_payment,"deposito")){
                $createSaleOrder["custbody_offline_pago_paypal"] = true;
                $createSaleOrder["custbody_paypal_amount"] = (double) 0;
                $createSaleOrder["custbody_saleorder_disable_edit"] = (Boolean) true;
            }else{
                $createSaleOrder["custbody_offline_pago_paypal"] = false;
                $createSaleOrder["custbody_paypal_amount"] = (double) $pedido->paypal_amount;
                $createSaleOrder["custbody_saleorder_disable_edit"] = (Boolean) false;
            }
        }

        if($pedido->paypal_code!='{}' && $pedido->paypal_code!="" || 
            ($pedido->cfdi_forma!=0 && $pedido->cfdi_forma!='') ||
            ($pedido->cfdi_method!=0 && $pedido->cfdi_method!=''))
        {
            $createSaleOrder["custbody_efx_fe_formapago"] = 22;
            $createSaleOrder["custbody_efx_fe_metodopago"] = 1;
        }
        else
        {
            $createSaleOrder["custbody_efx_fe_formapago"] = $pedido->cfdi_forma;
            $createSaleOrder["custbody_efx_fe_metodopago"] = $pedido->cfdi_method;
        }

        if($pedido->cfdi_uso != 0 && $pedido->cfdi_uso != ''){
            $createSaleOrder["custbody_efx_fe_usocfdi"] = $pedido->cfdi_uso;
        }
        if ($pedido->quote == 0) {
            $createSaleOrder["custbody10"] = true;
        }
        if($pedido->delivery == '4'){
            $createSaleOrder["custbody21"] = true;
            $createSaleOrder["custbody_adjuntar_guia_cliente"] = $pedido->documentId;
        }
        if($pedido->userFinal){
            $createSaleOrder["custbody_envio_entrega_usuario_final"] = true;
            $createSaleOrder["custbody_recibe_nombre"] = str_replace($search," ",$this->ns::wrapperAcentos($pedido->nameUserFinal));
            $createSaleOrder["custbody_recibe_telefono"] = $pedido->phoneUserFinal;
            $createSaleOrder["custbody_horario_entrega_desde"] = $pedido->startUserFinal;
            $createSaleOrder["custbody_horario_entrega_hasta"] = $pedido->endUserFinal;
        }
        if($address){
            $createSaleOrder["shipAddressList"] = ["id" => (int) $pedido->address_id];
            /*$createSaleOrder["shipAddress"] = ["id" => (int) $pedido->address_id, "addrText" => str_replace($search," ",$this->ns::wrapperAcentos($address->text))];
            $createSaleOrder["shippingAddressList"] = $createSaleOrder["shipAddressList"];
            $createSaleOrder["shippingAddress"] = $createSaleOrder["shipAddress"];*/
        }
            
        $createSaleOrder["otherRefNum"] = $pedido->customer_code."-".str_pad($pedido->id,6,"0",STR_PAD_LEFT);
        if($pedido->NoOrderCompra){
            $createSaleOrder["otherRefNum"] = $createSaleOrder["otherRefNum"]." (#OC: ".$pedido->NoOrderCompra.")";
        }

        if($pedido->FileOrderOc){
            $createSaleOrder["custbody43"] = $pedido->FileOrderOc;
        }

        $itemsPedido = json_decode($pedido->data_items);
        $secureSend = false;
        $items = [];

        foreach ($itemsPedido as $producto) {
            $discount = isset($producto->discount) ? $producto->discount : 0;
            $location = $pedido->location_id;
            $price = $producto->price ? $producto->price : 0;

            $item = Item::find($producto->item);
            //depende del accesor en el modelo para algunos casos especificos
            $typeItem = $item->type;

            if($typeItem || property_exists($producto, 'type')){
                if($typeItem == 4){
                    $getitemsGrupo = ItemMember::where('item_id', $producto->item)->get();
                    foreach ($getitemsGrupo as $item) {
                        $article=[
                            "item" => ["id" => $item->item_member],
                            "quantity" => (int)$producto->quantity * (int) $item->quantity,
                            "custcol_descuento_calculado" => $discount,
                            "location" => $producto->location,
                        ];
                        $article["price"] = $producto->price;
                        $article["taxCode"] = 21;
                        $items[]=$article;
                    }
                    continue;
                }

                $article=[
                    "item" => ["id" => $producto->item],
                    "quantity" => (int) $producto->quantity,
                    "custcol_descuento_calculado" => $discount,
                    "location" => $producto->location,
                    "price" => $producto->price,
                    "taxCode" => $producto->taxCode,
                ];

                if(property_exists($producto,'rate')){
                    $article["rate"] = floatval($producto->rate);
                    if($producto->item == '9038'){
                        $secureSend = true;
                        $article["custcol_shipping_insurance"] = true;
                        $article["custcol_original_custom_price"] = "{'rate':".floatval($producto->rate).",'currency':".$pedido->currency."}";
                    }
                    if(property_exists($producto,"type") && $producto->type == "cupon")
                    {
                        $article["custcol_original_custom_price"] = "{'rate':".floatval($producto->rate).",'currency':".$pedido->currency."}";
                        $article["custcol_cupon_aplicado"] = $producto->idCupon;
                    }
                }
                $items[]=$article;
            }
        }

        if($pedido->delivery == 1){
            $dataSend = json_decode($pedido->data_shipping);

            if(substr($pedido->data_shipping,0,7)!='{"costo' && strlen($pedido->data_shipping)>5)
            {
                $sendPackage = [];
                if($pedido->amount_shipping > 0.01){
                    $sendPackage = [
                        "item" => ["id" => $dataSend->costoEnvioItem],
                        "quantity" => 1,
                        "price" => -1,
                        "amount" => (double) $pedido->amount_shipping,
                        "rate" => (double) $pedido->amount_shipping,
                        "location" => $pedido->location_id,
                        "custcolis_shipping_cost" => true,
                        "custcol_precio_envio_data" => str_replace('"',"'",$pedido->data_shipping)
                    ];
                    if($secureSend){
                        $sendPackage["custcol_original_custom_price"] = "{'rate':".(double) $pedido->amount_shipping.",'currency':".$pedido->currency."}";
                    }
                    $items[]=$sendPackage;
                }
                $createSaleOrder["custbody_ped_paqueteria_id"] = $dataSend->guiaEmpresa;
                $createSaleOrder["custbody_ped_costo_envio_cargado"] = true;
                $createSaleOrder["custbody_data_calculo_envio"] = str_replace('"',"'",$pedido->data_shipping);
                $createSaleOrder["custbody_monto_costo_envio"] = (double) $pedido->amount_shipping;
                $createSaleOrder["custbody_codigo_postal_envio"] = $pedido->zip;
            }
        }

        $createSaleOrder["item"] = ["items" => $items];

        $orderJson = @json_encode($createSaleOrder);
        return $orderJson;
	}
}
