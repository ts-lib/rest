<?php

namespace TsLib\Rest\NsRest\Records\Support;

use TsLib\Rest\NsRest\Records\Record;

class supportCase extends Record
{
	protected $boolean = [
		'inactive' => 'isInactive',
	];

	protected $integer = [
		'status',
		'category',
		'assigned',
		'profile',
	];
}