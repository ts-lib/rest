<?php

namespace TsLib\Rest\NsRest\Records;
use TsLib\Rest\NsRest\NsBase;

class Record extends NsBase
{
	protected $rest_path = 'record/v1/';
}