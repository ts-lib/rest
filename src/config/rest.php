<?php

return [

	'token_key' => env('APP_NS_TOKEN_KEY', ''),
	'token_secret' => env('APP_NS_TOKEN_SECRET', ''),
	'consumer_key' => env('APP_NS_CONSUMER_KEY', ''),
	'consumer_secret' => env('APP_NS_CONSUMER_SECRET', ''),
	'account' => env('APP_NS_ACCOUNT', ''),

];