<?php

namespace TsLib\Rest\Services;

use Illuminate\Support\Facades\Log;
use TsLib\Models\NsRestletRequest;

class Curl
{
    protected $API_ROOT_URL = "";

    /**
     * Configuration for CURL
     */
    public static $CURL_OPTS = array(
        CURLOPT_USERAGENT => "Tecnosinergia Rest Lib", 
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_CONNECTTIMEOUT => 10, 
        CURLOPT_RETURNTRANSFER => true, 
        CURLOPT_TIMEOUT => 60,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLINFO_HEADER_OUT => true,
        CURLOPT_HEADER => true
    );

    protected $client_id;
    protected $client_secret;
    protected $redirect_uri;
    protected $access_token;
    protected $refresh_token;
    protected $auth;

    private $oauth_version = "1.0";
    private $oauth_signatureMethod = "HMAC-SHA256";

    protected $orig_opts = [];
    protected $opts = [];

    /**
     * Constructor method. Set all variables to connect
     *
     * @param string $client_id
     * @param string $client_secret
     * @param string $access_token
     * @param string $refresh_token
     */
    public function __construct($root_url, $auth = [], $extra_opts = []) {
        $this->API_ROOT_URL = $root_url;
        $this->auth = $auth;

        // array_merge destruye los indices del array, por eso tengo que hacer esto para unificarlos al instanciar
        foreach(self::$CURL_OPTS as $opt => $value)
        {
            $this->opts[$opt] = $value;
        }

        foreach($extra_opts as $opt => $value)
        {
            $this->opts[$opt] = $value;
        }
        $this->orig_opts = $this->opts;
    }

    /**
     * Execute a GET Request
     * 
     * @param string $path
     * @param array $params
     * @param boolean $assoc
     * @return mixed
     */
    public function get($path, $params = null, $assoc = false, $log = true) {
        $exec = $this->execute($path, [CURLOPT_CUSTOMREQUEST => 'GET'], $params, $assoc, $log);

        return $exec;
    }

    /**
     * Execute a POST Request
     * 
     * @param string $body
     * @param array $params
     * @return mixed
     */
    public function post($path, $body = null, $params = array(), $json = true, $assoc = false, $log = true) {
        $opts = [];
        if($json) {
            $body = json_encode($body);
            $opts[CURLOPT_HTTPHEADER] = ['Content-Type: application/json'];
        }

        $opts[CURLOPT_POST] = true;
        $opts[CURLOPT_CUSTOMREQUEST] = 'POST';
        $opts[CURLOPT_POSTFIELDS] = $body;

        $exec = $this->execute($path, $opts, $params, $assoc, $log);

        return $exec;
    }

    /**
     * Execute a POST Request of multipart data
     * 
     * @param string $body
     * @param array $params
     * @return mixed
     */
    public function post_multipart($path, $body = null, $params = array(), $assoc = false, $log = true) {
        $opts = array(
            CURLOPT_HTTPHEADER => array('content-type: multipart/form-data'),
            CURLOPT_POST => true, 
            CURLOPT_POSTFIELDS => $body
        );

        $exec = $this->execute($path, $opts, $params, $assoc, $log);

        return $exec;
    }

    /**
     * Execute a PUT Request
     * 
     * @param string $path
     * @param string $body
     * @param array $params
     * @return mixed
     */
    public function put($path, $body = null, $params, $json = false, $assoc = false, $log = true) {
        if($json)
            $body = json_encode($body);

        $opts = array(
            CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => $body
        );
        
        $exec = $this->execute($path, $opts, $params, $assoc, $log);

        return $exec;
    }

    /**
     * Execute a DELETE Request
     * 
     * @param string $path
     * @param array $params
     * @return mixed
     */
    public function delete($path, $params = [], $assoc = false, $log) {
        $opts = array(
            CURLOPT_CUSTOMREQUEST => "DELETE"
        );
        
        $exec = $this->execute($path, $opts, $params, $assoc, $log);
        
        return $exec;
    }

    /**
     * Execute a OPTION Request
     * 
     * @param string $path
     * @param array $params
     * @return mixed
     */
    public function options($path, $params = null) {
        $opts = array(
            CURLOPT_CUSTOMREQUEST => "OPTIONS"
        );
        
        $exec = $this->execute($path, $opts, $params);

        return $exec;
    }

    private function set_auth_options(&$ch) {
        if($this->auth == [] || !isset($this->auth['type'])){
            return $ch;
        }

        if($this->auth['type'] == 'basic'){
            if(!isset($this->auth['user']) || !isset($this->auth['password'])){
                return $ch;
            }
            curl_setopt($ch, CURLOPT_USERPWD, sprintf('%s:%s', $this->auth['user'], $this->auth['password']));
            return $ch;
        }

        if($this->auth['type'] == 'header_bearer')
        {
            if(!isset($this->auth['token']) || trim($this->auth['token']) == '')
            {
                return $ch;
            }
            $this->opts[CURLOPT_HTTPHEADER][] = 'Authorization: Bearer '.$this->auth['token'];
            return $ch;
        }

        if($this->auth['type'] == 'header_token')
        {
            if(!isset($this->auth['token']) || trim($this->auth['token']) == '')
            {
                return $ch;
            }
            $this->opts[CURLOPT_HTTPHEADER][] = 'authorization: Token '.$this->auth['token'];
            return $ch;
        }

        if($this->auth['type'] == 'header_auth')
        {
            if(!isset($this->auth['token']) || trim($this->auth['token']) == '')
            {
                return $ch;
            }
            $this->opts[CURLOPT_HTTPHEADER][] = 'authorization: '.$this->auth['token'];
            return $ch;
        }

        if($this->auth['type'] == 'api_token')
        {
            if(!isset($this->auth['token']) || trim($this->auth['token']) == '')
            {
                return $ch;
            }
            $this->opts[CURLOPT_HTTPHEADER][] = 'api-token: '.$this->auth['token'];
            return $ch;
        }

        if($this->auth['type'] == 'oauth')
        {
            $this->opts[CURLOPT_HTTPHEADER][] = $this->get_oauth();
            return $ch;
        }
    }

    private function get_oauth()
    {
        if(!isset($this->opts[CURLOPT_URL]))
        {
            Log::error('No esta definida la URL en las opciones para hacer cURL');
            return '';
        }
        $url = $this->opts[CURLOPT_URL];

        if(!isset($this->opts[CURLOPT_CUSTOMREQUEST]))
        {
            Log::error('No esta definida el Metodo en las opciones para hacer cURL');
            return '';
        }
        $httpMethod = $this->opts[CURLOPT_CUSTOMREQUEST];

        $nonce = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);
        $timestamp = time();

        $url_info = parse_url($url);
        if(!isset($url_info['host']))
        {
            Log::error(sprintf('Se intentó autenticar con OAUTH con %s que no es una URL valida (no tiene host)', $url));
            return '';
        }
        $baseUrl = vsprintf('%s://%s%s%s', [
            isset($url_info['scheme']) ? $url_info['scheme'] : 'https',
            $url_info['host'],
            isset($url_info['port']) ? ':'.$url_info['port'] : '',
            isset($url_info['path']) ? $url_info['path'] : '',
        ]);

        $getParams = isset($url_info['query']) ? $url_info['query'] : '';

        $baseString = strtoupper($httpMethod).'&'.rawurlencode($baseUrl).'&';

        $params = [];
        if(trim($getParams) != '' )
        {
            $params_array = explode('&', $getParams);
            foreach($params_array as $par)
            {
                $separated = explode('=', $par);
                $params[urldecode($separated[0])] = urldecode($separated[1]);
            }
        }

        $params['oauth_consumer_key'] = config('ts-lib-rest.consumer_key');
        $params['oauth_nonce'] = $nonce;
        $params['oauth_signature_method'] = $this->oauth_signatureMethod;
        $params['oauth_timestamp'] = $timestamp;
        $params['oauth_token'] = config('ts-lib-rest.token_key');
        $params['oauth_version'] = $this->oauth_version;
        ksort($params);

        $baseString .= rawurlencode(http_build_query($params, '', '&', PHP_QUERY_RFC3986));

        $key = rawurlencode(config('ts-lib-rest.consumer_secret')) .'&'. rawurlencode(config('ts-lib-rest.token_secret'));

        $signature = base64_encode(hash_hmac('sha256', $baseString, $key, true));

        return 'Authorization: OAuth '
                        .'realm="' .rawurlencode(config('ts-lib-rest.account')) .'", '
                        .'oauth_consumer_key="' .rawurlencode(config('ts-lib-rest.consumer_key')) .'", '
                        .'oauth_token="' .rawurlencode(config('ts-lib-rest.token_key')) .'", '
                        .'oauth_nonce="' .rawurlencode($nonce) .'", '
                        .'oauth_timestamp="' .rawurlencode($timestamp) .'", '
                        .'oauth_signature_method="' .rawurlencode($this->oauth_signatureMethod) .'", '
                        .'oauth_version="' .rawurlencode($this->oauth_version) .'", '
                        .'oauth_signature="' .rawurlencode($signature) .'"';
    }

    public function geturl()
    {
        return $this->API_ROOT_URL;
    }

    /**
     * Execute all requests and returns the json body and headers
     * 
     * @param string $path
     * @param array $opts
     * @param array $params
     * @param boolean $assoc
     * @return mixed
     */
    public function execute($path, $opts = array(), $params = [], $assoc = false, $log = true) {
        $uri = $this->make_path($path, $params);

        // array_merge destruye los indices del array, por eso tengo que hacer esto para unificarlos al instanciar
        // tambien aqui le doy chance de agregar mas opciones al llamar la funcion, como cuando se agrega CURLOPT_CUSTOMREQUEST
        $this->opts = $this->orig_opts;
        foreach($opts as $opt => $value)
        {
            if(isset($this->orig_opts[$opt]) && is_array($this->orig_opts[$opt]))
            {
                $this->opts[$opt] = array_merge($this->orig_opts[$opt], $value);
            }
            else
            {
                $this->opts[$opt] = $value;
            }
        }

        $log_request = new NsRestletRequest();
        $log_request->url = $this->API_ROOT_URL;
        $log_request->path = $path;
        $log_request->query = json_encode($params);

        $url_info = parse_url($uri);
        if(isset($url_info['query']))
        {
            $out = [];
            parse_str($url_info['query'], $out);
            if(isset($out['script']))
            {
                $log_request->script_id = $out['script'];
            }
        }
        
        $log_request->method = $this->opts[CURLOPT_CUSTOMREQUEST];
        $log_request->status = 0;
        if(isset($this->opts[CURLOPT_POSTFIELDS]))
        {
            $log_request->json = $this->opts[CURLOPT_POSTFIELDS];
        }
        else
        {
            $log_request->json = '';
        }

        $this->opts[CURLOPT_URL] = $uri;

        $ch = curl_init();

        $this->set_auth_options($ch);

        curl_setopt_array($ch, $this->opts);

        $data = curl_exec($ch);
        $response_info = curl_getinfo($ch);
        $req_headers = explode(PHP_EOL, $response_info['request_header']);
        $return['request_header'] = $req_headers;

        if($data === false)
        {
            $log_request->response = curl_error($ch);
            $log_request->status = 1;
            $log_request->save();
            curl_close($ch);
            return ["status" => false, "httpCode" => 500, "body" => [], "message"=>"Error curl", 'headers' => [], 'request_header' => $req_headers, 'uri' => $uri];
        }

        if(($response_content_type_header = curl_getinfo($ch, CURLINFO_CONTENT_TYPE)) != false)
        {
            $response_content_type = explode(';', $response_content_type_header);
            $parts_rct = explode('/', $response_content_type[0]);
            $response_content_type[0] = explode('+', $parts_rct[1]);
        }
        else
        {
            $response_content_type[0] = [];
        }

        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $return["httpCode"] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $header = substr($data, 0, $headerSize);
        $body = substr($data, $headerSize);

        if($response_content_type[0] == 'json' || in_array('json', $response_content_type[0]))
            $return["body"] = json_decode($body, $assoc);
        else
            $return["body"] = $body;

        $arrayHead = explode("\\r\\n",json_encode($header));
        $responseHeaders = [];

        foreach ($arrayHead as $head) {
            $parts = explode(':', $head);
            if(isset($parts[1]))
            {
                $responseHeaders[trim($parts[0])] = trim($parts[1]);
            }
            else
            {
                $responseHeaders[] = trim($head);
            }
        }
        $return['headers'] = $responseHeaders;
        $return['uri'] = $uri;

        $log_request->response = json_encode($return);
        if(!in_array($return["httpCode"], [200, 201, 202, 203]))
        {
            $log_request->status = 2;
            $log_request->save();
            return $return;
        }
        
        if($return["body"] === false)
        {
            $log_request->status = 1;
            $log_request->save();
            return $return;
        }
        
        $log_request->status = 9;
        if($log)
        {
            $log_request->save();
        }

        return $return;
    }

    /**
     * Check and construct an real URL to make request
     * 
     * @param string $path
     * @param array $params
     * @return string
     */
    public function make_path($path, $params = []) {
        if (!preg_match("/^http/", $path)) {
            if (!preg_match("/^\//", $path) && !preg_match("/^\?/", $path)) {
                $path = '/'.$path;
            }
            $uri = $this->API_ROOT_URL.$path;
        } else {
            $uri = $path;
        }

        if(!empty($params)) {
            $uri .= '?'.http_build_query($params, '', null, PHP_QUERY_RFC3986);
        }

        return $uri;
    }
}