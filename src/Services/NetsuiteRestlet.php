<?php

namespace TsLib\Rest\Services;

use TsLib\Rest\Models\NsRestlet;
use Illuminate\Support\Facades\Log;

class NetsuiteRestlet extends NetsuiteBase {

    public function __construct()
    {
        $this->curl = new Curl(sprintf("https://%s.restlets.api.netsuite.com/app/site/hosting/restlet.nl", strtolower(str_replace('_', '-', config('ts-lib-rest.account')))), ['type' => 'oauth']);
    }

    public function restlet($httpMethod, $name, $params, $json='', $log = true)
    {
        $path = $this->make_restlet_query($name, $params);

        return $this->doRequest($httpMethod, '?'.$path, $json, $log);
    }

    private function make_restlet_query($name, $params = [])
    {
        $restlet = NsRestlet::where('name', $name)->first();
        if($restlet == null)
        {
            Log::error(sprintf("Se intento llamar al Restlet %s pero no tiene registro en ns_restlets", $name));
            return false;
        }

        if(strpos(config('ts-lib-rest.account'), '_') === false)
        {
            $script_id = $restlet->script_id;
        }
        else
        {
            $script_id = $restlet->script_id_sb;
        }

        if ($restlet->has_debug_deploy === 0)
        {
            $deploy = 1;
        }
        else
        {
            $deploy = $restlet->has_debug_deploy;
        }

        $query = array_merge([
                "script" => $script_id,
                "deploy" => $deploy,
            ], $params);

        return http_build_query($query);
    }

    protected function doRequest($httpMethod, $path, $json = "", $log = true)
    {
        $resp = ["status" => false,"data" => [],"message"=>"Error api get"];
        $result = parent::doRequest($httpMethod, $path, $json, $log);

        if(!in_array($result["httpCode"], [200, 201, 202, 203]))
        {
            $resp["data"] = [];
            $resp['message'] = (isset($result['body']->error->message)) ? $result['body']->error->message : "No se recibio un resultado exitoso";
            $resp["status"] = false;

            return $resp;
        }

        if(isset($result['body']->code) && $result['body']->code)
        {
            if(isset($result['body']->data))
            {
                $resp["data"] = $result['body']->data;
            }
            else
            {
                $resp["data"] = [];
            }
            $resp['message'] = $result['body']->message;
            $resp["status"] = true;
        }
        else
        {
            $resp["status"] = false;
            if(isset($result["body"]->message))
            {
                $resp["message"] = $result["body"]->message;
            }

            if(isset($result["body"]->data))
            {
                $resp['data'] = $result["body"]->data;
            }
        }

        return $resp;
    }
}