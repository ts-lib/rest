<?php

namespace TsLib\Rest\Services;

use Curl;

use Illuminate\Support\Facades\Log;

class NetsuiteBase {

    protected $curl = null;

    public function __construct()
    {
        date_default_timezone_set(config('app.timezone', 'America/Mexico_City'));
    }

    public function get($path, $params = [], $log = true)
    {
        return $this->curl->get($path, $params, $log);
    }

    public function post($path, $json, $log = true)
    {
        return $this->curl->post($path, $json, $log);
    }

    protected function doRequest($httpMethod,$path,$json="", $log = true)
    {
        if($httpMethod == "POST"){
            $validateJSON = $this->_jsonValidator($json);
            if($validateJSON != 1){
                return ["status" => false, "httpCode" => 0, "data" => [], "message"=>"Error NetSuiteBase: No body", 'headers' => []];
            }
        }

        $options = [
            CURLOPT_CUSTOMREQUEST => $httpMethod,
            CURLOPT_HTTPHEADER => [
                'Content-type:application/json'
            ],
            CURLOPT_HEADER => 1
        ];

        if($httpMethod == "POST")
        {
            $options[CURLOPT_POSTFIELDS] = $json;
        }

        //hacer CURL
        return $this->curl->execute($path, $options, [], false, $log);
    }

    public function _jsonValidator($json)
    {
        if (!empty($json)) {
            @json_decode($json);
            return (json_last_error() === JSON_ERROR_NONE);
        }
        return false;
    }

    public function _wrapperAcentos($text="")
    {
        return self::wrapperAcentos($text);
    }

    static public function wrapperAcentos($text="")
    {
        return htmlentities(html_entity_decode(utf8_encode($text)));
    }

    protected function _getOauth($httpMethod, $url)
    {
        $nonce = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);
        $timestamp = time();

        if (strpos($url, '?')){  
            $baseUrl = substr($url, 0, strpos($url, '?'));
            $getParams = substr($url, strpos($url, '?') + 1);
        } else {
            $baseUrl = $url;
            $getParams = "";
        }

        $restlet = [];
        if($getParams!=""){
            $restlet = explode('&',$getParams);
        }

        $baseString = strtoupper($httpMethod).'&'.rawurlencode($baseUrl).'&';

        $params = $values = [];
        foreach($restlet as $par){
            $val = explode("=",$par);
            $values[]=$val[0];
        }
       
        if(in_array("customer_id",$values)){
            $params["customer_id"] = substr($restlet[2],strpos($restlet[2], '=')+1,10);
        }
        if(in_array("deploy",$values)){
            $params["deploy"] = substr($restlet[1],strpos($restlet[1], '=')+1,10);
        }
        if(in_array("month",$values)){
            $params["month"] = substr($restlet[3],strpos($restlet[3], '=')+1,10);
        }
        $params['oauth_consumer_key'] = config('ts-lib-rest.consumer_key');
        $params['oauth_nonce'] = $nonce;
        $params['oauth_signature_method'] = "HMAC-SHA256";
        $params['oauth_timestamp'] = $timestamp;
        $params['oauth_token'] = config('ts-lib-rest.token_key');
        $params['oauth_version'] = "1.0";
        if(in_array("script",$values)){
            $params["script"] = substr($restlet[0],strpos($restlet[0], '=')+1,10);
        }
        if(in_array("tranid",$values)){
            $params["tranid"] = substr($restlet[2],strpos($restlet[2], '=')+1,10);
        }
        if(in_array("year",$values)){
            $params["year"] = substr($restlet[4],strpos($restlet[4], '=')+1,10);
        }
        
        $paramString = "";
        foreach ($params as $key => $value){
            $paramString .= rawurlencode($key) . '='. rawurlencode($value) .'&';
        }
        $paramString = substr($paramString, 0, -1);

        $baseString .= rawurlencode($paramString);

        $key = rawurlencode(config('ts-lib-rest.consumer_secret')) .'&'. rawurlencode(config('ts-lib-rest.token_secret'));

        $signature = base64_encode(hash_hmac('sha256', $baseString, $key, true));

        return 'Authorization: OAuth '
                        .'realm="' .rawurlencode(config('ts-lib-rest.account')) .'", '
                        .'oauth_consumer_key="' .rawurlencode(config('ts-lib-rest.consumer_key')) .'", '
                        .'oauth_token="' .rawurlencode(config('ts-lib-rest.token_key')) .'", '
                        .'oauth_nonce="' .rawurlencode($nonce) .'", '
                        .'oauth_timestamp="' .rawurlencode($timestamp) .'", '
                        .'oauth_signature_method="' .rawurlencode("HMAC-SHA256") .'", '
                        .'oauth_version="' .rawurlencode("1.0") .'", '
                        .'oauth_signature="' .rawurlencode($signature) .'"';
    }
}