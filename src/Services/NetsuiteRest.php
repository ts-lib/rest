<?php

namespace TsLib\Rest\Services;

class NetsuiteRest extends NetsuiteBase {

    public function __construct()
    {
        $this->curl = new Curl(sprintf('https://%s.suitetalk.api.netsuite.com/services/rest/', strtolower(str_replace('_', '-', config('ts-lib-rest.account')))), ['type' => 'oauth']);
    }

    protected function doRequest($httpMethod, $path, $json = "", $log = true)
    {
        $resp = ["status" => false,"data" => [],"message"=>"Error api get"];
        $result = parent::doRequest($httpMethod, $path, $json, $log);

        if(!in_array($result["httpCode"], [200, 201, 202, 203]))
        {
            $resp["data"] = [];
            $resp['message'] = [];
            foreach($result['body']->{'o:errorDetails'} as $error)
            {
                $resp['message'][] = [$error->{'o:errorCode'} => $error->detail];
            }
            $resp["status"] = false;

            return $resp;
        }

        return [
            'status' => true,
            'message' => '',
            'data' => $result['body'],
        ];
    }
}